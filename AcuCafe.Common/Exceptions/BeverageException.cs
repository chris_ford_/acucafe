﻿using System;

namespace AcuCafe.Common
{
    public class BeverageException : Exception
    {
        public BeverageException()
            : base()
        {
        }

        public BeverageException(string message)
            : base(message)
        {
        }

    }
}
