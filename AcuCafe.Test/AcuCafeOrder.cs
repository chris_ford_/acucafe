﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace AcuCafe.Test
{
    using System.Linq;

    using AcuCafeConsole.Domain;
    using AcuCafeConsole.Model;

    [TestClass]
    public class AcuCafeOrder
    {
        private Cart _cart;
        [TestInitialize]
        public void Setup()
        {
            _cart = new Cart();
        }

        [TestMethod]
        public void CalculateCostofLargeIcedTea()
        {
            Tea icedTea = new Tea(Size.Large, BeverageType.EarlGrey, Toppings.None, true, false, false);
            _cart.Add(icedTea);
            Assert.AreEqual(4.5m, _cart.TotalAmount());
        }

        [TestMethod]
        public void TwoItemsInBasket()
        {
            Tea icedTea = new Tea(Size.Large, BeverageType.EarlGrey, Toppings.None, true, false, false);
            _cart.Add(icedTea);
            Coffee coffee = new Coffee(Size.Large, BeverageType.Espresso, Toppings.None, false, false, false);
            _cart.Add(coffee);

            Assert.AreEqual(2, _cart.Items.Count());
        }

        [TestMethod]
        public void EspressoWithChocolateTopping()
        {
            var coffee = new Coffee(Size.Large, BeverageType.Espresso, Toppings.Chocolate, true, false, false);
            _cart.Add(coffee);
            var order = new Order(_cart);
            Assert.AreEqual("Preparing your Large cup of Espresso with Chocolate topping for £5.00", order.Checkout());
            
        }

        [TestMethod]
        public void IcedTeaWithMilkError()
        {
            var tea = new Tea(Size.Large, BeverageType.BreakfastTea, Toppings.Chocolate, true, false, true);
            _cart.Add(tea);
            var order = new Order(_cart);
            Assert.AreEqual("Please inform the client this is an invalid option.", order.Checkout());

        }
    }
}
