﻿using System.Text;

namespace AcuCafeConsole.Domain
{
    using AcuCafe.Common;

    using AcuCafeConsole.Model;
    using AcuCafeConsole.Model.Interface;
    /// <summary>
    /// Class used to hold global brew rules
    /// </summary>
    public class BuildBrewRules
    {
        public StringBuilder BuildBrewLogic(IBeverage beverage)
        {
            //Perform basic sanity checks
            //Following the fail hard and fast principle.
            if (beverage.GetType() == typeof(Coffee))
            {
                switch (beverage.BeverageType)
                {
                    case BeverageType.BreakfastTea:
                    case BeverageType.EarlGrey:
                        throw new BeverageException("Please inform the customer they have this is an invalid order.");
                    default:
                        break;
                }
            }
            else
            {
                switch (beverage.BeverageType)
                {
                    case BeverageType.Americano:
                    case BeverageType.Espresso:
                    case BeverageType.Latte:
                        throw new BeverageException("Please inform the customer they have this is an invalid order.");
                    default:
                        break;
                }
            }

            StringBuilder message = new StringBuilder();
            message.Append("Preparing your ");
            switch (beverage.CupSize)
            {
                case Size.Pot:
                    message.Append("Pot of ");
                    break;
                default:
                    message.Append(beverage.CupSize + " cup of ");
                    break;
            }
            message.Append(beverage.BeverageType);

            if (beverage.Milk)
            {
                message.Append(" with milk");
            }
            return message;
            
        }
    }
}
