﻿namespace AcuCafeConsole.Domain
{
    using System.Collections.Generic;
    using System.Linq;

    using AcuCafeConsole.Domain.Interface;
    using AcuCafeConsole.Model.Interface;

    public class Cart
    {
        private readonly List<IBeverage> _items;
        private readonly IPricingCalculator _pricingCalculator;

        public Cart()
            : this(new PricingCalculator())
        {
        }

        public Cart(IPricingCalculator pricingCalculator)
        {
            _pricingCalculator = pricingCalculator;
            _items = new List<IBeverage>();
        }

        public IEnumerable<IBeverage> Items
        {
            get
            {
                return _items;
            }
        }

        public void Add(IBeverage orderItem)
        {
            _items.Add(orderItem);
        }

        public decimal TotalAmount()
        {
            return this.Items.Sum(orderItem => this._pricingCalculator.CalculatePrice(orderItem));
        }
    }
}
