﻿namespace AcuCafeConsole.Domain
{
    using AcuCafeConsole.Domain.Interface;
    using AcuCafeConsole.Model;
    using AcuCafeConsole.Model.Interface;

    public class PricingCalculator : IPricingCalculator
    {
        public decimal CalculatePrice(IBeverage beverage)
        {
            var totalPrice = 0m;

            if (beverage.CupSize == Size.Pot)
            {
                totalPrice = 9.00m;
            }
            else if (beverage.CupSize == Size.Large)
            {
                totalPrice = 4.00m;
            }
            else if (beverage.CupSize == Size.Medium)
            {
                totalPrice = 3.00m;
            }
            else
            {
                totalPrice = 2.00m;
            }
            if (beverage.Iced)
            {
                totalPrice += 0.5m;
            }
            if (beverage.Toppings == Model.Toppings.Chocolate)
            {
                totalPrice += 0.5m;
            }
            else if (beverage.Toppings == Toppings.HundredsAndThousands)
            {
                totalPrice += 0.2m;
            }
            else if (beverage.Toppings == Toppings.Marshmallows)
            {
                totalPrice += 0.7m;
            }

            return totalPrice;
        }
    }
}
