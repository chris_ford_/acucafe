﻿namespace AcuCafeConsole.Model
{
    /// <summary>
    /// List of available beverage types.
    /// </summary>
    public enum BeverageType
    {
        Espresso,
        Latte,
        BreakfastTea,
        EarlGrey,
        Americano
    }
}
