﻿namespace AcuCafeConsole.Model
{
    public enum Size
    {
        Pot,
        Large,
        Medium,
        Small
    }
}
