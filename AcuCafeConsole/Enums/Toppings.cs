﻿namespace AcuCafeConsole.Model
{
    /// <summary>
    /// Different Topping Types
    /// </summary>
    public enum Toppings
    {
        Chocolate,
        HundredsAndThousands,
        Marshmallows,
        None
    }
}
