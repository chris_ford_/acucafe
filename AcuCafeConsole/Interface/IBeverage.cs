﻿namespace AcuCafeConsole.Model.Interface
{
    using System.Text;

    public interface IBeverage
    {

        Size CupSize { get; }

        Toppings Toppings { get;  }

        bool Sweetened { get; }

        bool Milk { get; }

        bool Iced { get;  }

        BeverageType BeverageType { get; }

        StringBuilder BrewBeverage(IBeverage beverage);

    }
}
