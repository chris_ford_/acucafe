﻿namespace AcuCafeConsole.Domain.Interface
{
    using AcuCafeConsole.Model.Interface;

    public interface IPricingCalculator
    {
        decimal CalculatePrice(IBeverage beverage);
    }
}
