﻿namespace AcuCafeConsole.Model
{
    using System.Text;

    using AcuCafe.Common;

    using AcuCafeConsole.Domain;
    using AcuCafeConsole.Model.Interface;

    public abstract class Beverage : IBeverage
    {
        /// <summary>
        /// Gets or sets the cup size.
        /// </summary>
        public Size CupSize { get; private set; }

        /// <summary>
        /// Gets or sets the toppings.
        /// </summary>
        public Toppings Toppings { get; private set; }

        /// <summary>
        /// Gets or sets whether sweetener is included
        /// </summary>
        public bool Sweetened { get; private set; }

        /// <summary>
        /// Gets or sets whether to add milk
        /// </summary>
        public bool Milk { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public BeverageType BeverageType { get; private set; }

        public bool Iced { get; private set; }

        public decimal CustomerPrice { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Beverage"/> class. 
        /// Default parameterless Constructor
        /// </summary>
        public Beverage()
        {
            
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Beverage"/> class.
        /// </summary>
        /// <param name="size">
        /// The size.
        /// </param>
        /// <param name="beverageType">
        /// The beverage Type.
        /// </param>
        /// <param name="toppings">
        /// The toppings.
        /// </param>
        /// <param name="iced">
        /// The iced.
        /// </param>
        /// <param name="sweetened">
        /// The sweetened.
        /// </param>
        /// <param name="milk">
        /// The milk.
        /// </param>
        public Beverage(Size size, BeverageType beverageType, Toppings toppings, bool iced, bool sweetened, bool milk)
        {
            this.CupSize = size;
            this.Iced = iced;
            this.BeverageType = beverageType;
            this.Sweetened = sweetened;
            this.Milk = milk;
            this.Toppings = toppings;
        }

        /// <summary>
        /// Base logic for brew method
        /// </summary>
        public virtual StringBuilder BrewBeverage(IBeverage beverage)
        {
            var brewRules = new BuildBrewRules();
            return brewRules.BuildBrewLogic(beverage);
        }
    }
}
