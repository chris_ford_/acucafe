﻿using System.Text;

namespace AcuCafeConsole.Model
{
    using AcuCafeConsole.Model.Interface;

    public class Coffee : Beverage
    {
        private IBeverage coffee;
        public Coffee(Size cupSize, BeverageType beverageType, Toppings toppings, bool iced, bool sweetened, bool milk) 
            : base(cupSize, beverageType, toppings, iced, sweetened, milk)
        {
            this.CupSize = cupSize;
            this.BeverageType = beverageType;
            this.Iced = iced;
            this.Milk = milk;
            this.Sweetened = sweetened;
            this.Toppings = toppings;
        }

        private Size CupSize { get; set; }

        private Toppings Toppings { get; set; }

        private bool Sweetened { get; set; }

        private bool Milk { get; set; }

        private bool Iced { get; set; }

        private BeverageType BeverageType { get; set; }

        public override StringBuilder BrewBeverage(IBeverage beverage)
        {
            StringBuilder message = new StringBuilder();
            message = base.BrewBeverage(beverage);

            if (beverage.Toppings != Toppings.None)
            {
                message.Append(" with " + beverage.Toppings + " topping");
            }

            return message;
        }
    }
}
