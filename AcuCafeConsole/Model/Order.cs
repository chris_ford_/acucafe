﻿
namespace AcuCafeConsole.Model
{
    using System;
    using System.Text;

    using AcuCafeConsole.Domain;

    public class Order
    {
        protected readonly Cart _cart;

        private string orderMessage;

        public Order(Cart cart)
        {
            _cart = cart;
        }

        public virtual string Checkout()
        {
            string message = string.Empty;
            try
            {
                message = this.ProcessOrder();
                message += " for " + _cart.TotalAmount().ToString("c");
                return message;
            }
            catch (Exception ex)
            {
                message = ex.Message;
                return message;
            }
        }

        private string ProcessOrder()
        {
            var message = new StringBuilder();
            foreach (var beverage in _cart.Items)
            {
                message = beverage.BrewBeverage(beverage);
            }
            return message.ToString();
        }
    }
}
