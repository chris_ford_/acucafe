﻿namespace AcuCafeConsole.Model
{
    using System;
    using System.Text;

    using AcuCafe.Common;

    using AcuCafeConsole.Model.Interface;

    public class Tea : Beverage
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Tea"/> class.
        /// </summary>
        /// <param name="cupSize">
        /// The cup size.
        /// </param>
        /// <param name="blendType">
        /// The blend type.
        /// </param>
        /// <param name="iced">
        /// The iced.
        /// </param>
        public Tea(Size cupSize, BeverageType beverageType, Toppings toppings, bool iced, bool sweetened, bool milk) 
            : base(cupSize, beverageType, toppings, iced, sweetened, milk)
        {
            this.CupSize = cupSize;
            this.BeverageType = beverageType;
            this.Iced = iced;
            this.Milk = milk;
            this.Sweetened = sweetened;
            this.Toppings = toppings;
        }

        private Size CupSize { get; set; }

        private Toppings Toppings { get; set; }

        private bool Sweetened { get; set; }

        private bool Milk { get; set; }

        private bool Iced { get; set; }

        private BeverageType BeverageType { get; set; }

        public override StringBuilder BrewBeverage(IBeverage beverage)
        {
            StringBuilder message = new StringBuilder();
            message = base.BrewBeverage(beverage);

            if (Iced)
            {
                if (Milk)
                {
                    throw new BeverageException("Please inform the client this is an invalid option.");
                }
            }
            return message;
        }
    }
}
