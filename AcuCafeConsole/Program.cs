﻿using System;

namespace AcuCafeConsole
{
    using AcuCafeConsole.Domain;
    using AcuCafeConsole.Model;

    class Program
    {
        private Coffee coffee;

        static void Main(string[] args)
        {
            var cart = new Cart();
            var coffee = new Coffee(Size.Large, BeverageType.Americano, Toppings.Chocolate, false, true, true);
            var tea = new Tea(Size.Pot, BeverageType.EarlGrey, Toppings.Chocolate, true, true, true);
            cart.Add(coffee);
            cart.Add(tea);

            var order = new Order(cart);
            
            order.Checkout();

            Console.Read();
        }
    }
}
