If I was building this to slightly different requirements I would use the builder design pattern.
This would build up more complex objects of the beverage types which would restrict the available options for the beverage types (for example adding milk with iced tea would not be an available option).
I would also use the strategy pattern to build up a more customisable and extendable set of rules.

If the logic and scope of the project was also to increase I would look at building additional projects to represent the Domain Layer and Repository (Data Access) layer.

Thank you for your time

Chris Ford